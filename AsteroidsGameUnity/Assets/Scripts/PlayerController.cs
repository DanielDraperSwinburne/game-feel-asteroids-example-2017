﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour 
{
	public float moveForce = 2f;
	public float rotateSpeed = 5f;

	public GameObject BulletPrefab;
	public float fireRate = 0.8f;
	public float bulletSpeed = 5f;

	private Rigidbody2D _rb;
	private Transform _transform;
	private SpriteRenderer _renderer;
	private Collider2D _col;
	private Vector3 velocity;

	private float lastFired = 0;

	public Sprite ColorSprite;
	public ParticleSystem Explosion;
	public ParticleSystem FancyExplosion;

	private TrailRenderer _trail;
	public ParticleSystem ThrusterParticleSystem;

	private DemoDataObject DemoData;

	public AudioSource DeathAudioSource;
	public AudioClip BassBoom;

	public GameObject BoomEffectPrefab;
	private CircleCollider2D boomCollider;
	public float BoomCooldown = 30f;
	private float nextBoomTime;
	public GameObject OutlineObject;
	public Image MyOutlineImage;
	private float OutlineValue = 0;
	private Color myOutlineColor;

	void Awake()
	{
		//get local references to all neccisary components on this object.
		_rb = GetComponent<Rigidbody2D>();
		_transform = transform;
		_renderer = GetComponent<SpriteRenderer>();
		_col = GetComponent<Collider2D>();
		_trail = GetComponent<TrailRenderer>();
	}
	// Use this for initialization
	void Start () 
	{
		DemoData = GameController.Instance.DemoData;

		_renderer.sprite = DemoData.Colour ? ColorSprite : _renderer.sprite;
		DoDemoMods();

		if (DemoData.Animations)
		{
			Vector3 curScale = transform.localScale;
			transform.localScale = Vector3.zero;
			LeanTween.scale(gameObject,curScale,0.5f).setEaseOutBounce();
		}
		if (DemoData.MoreBass) DeathAudioSource.clip = BassBoom;

		OutlineObject.SetActive(DemoData.SlowDown);
		myOutlineColor = MyOutlineImage.color;
		nextBoomTime = Time.time + BoomCooldown;
	}

	// Update is called once per frame
	void Update () 
	{
		
		DoMovement();
		DoWeapon();

		//Camera.main.transform.position = new Vector3(_transform.position.x,_transform.position.y,-10f);
		OutlineValue = 1-((nextBoomTime-Time.time)/BoomCooldown);
		MyOutlineImage.fillAmount = Mathf.Clamp01(OutlineValue);
		if (OutlineValue >= 1f)
		{
			MyOutlineImage.color = Color.Lerp(myOutlineColor,Color.white,OutlineValue-0.2f);
		}
	}

	void FixedUpdate()
	{
		//_rb.velocity = velocity;
	}

	void DoMovement()
	{
		//Change the Players roatation (z Axis) in the direction they are holding and at rotate speed
		transform.eulerAngles = new Vector3(0,0,transform.eulerAngles.z+(Input.GetAxis("Horizontal")*(-rotateSpeed*Time.deltaTime)));
		
		if (Input.GetButton("Vertical"))
			_rb.AddForce((Vector2)transform.right*(Input.GetAxis("Vertical")*(moveForce*Time.deltaTime)));

		//_anim.SetFloat("velocity",velocity.magnitude);
		float mag = _rb.velocity.magnitude; //get out speed
		var main = ThrusterParticleSystem.main;
		var emmision = ThrusterParticleSystem.emission; //get the main and emmision modules of the particle system
		main.startSpeed = mag; //set the start speed of each particle to our speed
		emmision.rateOverTime = mag*20; //set the emmision rate to 20 times our speed
	}

	void DoWeapon()
	{

		if (Input.GetButton("Fire1") && lastFired <= Time.time)
		{
			//pew pew pew
			GameObject Bullet = (GameObject)Instantiate(BulletPrefab,_transform.position+_transform.right*0.2f,transform.rotation);

			//Calculate bullet accuracy
			Vector2 bulletVel = ((Vector2)(_transform.right*bulletSpeed) + Random.insideUnitCircle * (DemoData.Inaccuracy ? 0.8f : 0f)).normalized*bulletSpeed;

			Bullet.GetComponent<Rigidbody2D>().velocity = bulletVel;

			if (DemoData.Recoil)
			{
				_rb.AddForce(_transform.right*-100f); ;
				ScreenEffects.Instance.Shake(-_transform.right*0.05f);
			}

			lastFired = Time.time + fireRate;
		}

		if (Input.GetButton("Fire2") && Time.time >= nextBoomTime && DemoData.SlowDown)
		{
			//pew pew pew
			GameObject Boom = (GameObject)Instantiate(BoomEffectPrefab,_transform.position,Quaternion.identity);

			boomCollider =  Boom.GetComponent<CircleCollider2D>();
			StartCoroutine(ScaleBoom());
			_col.enabled = false;

			Destroy(Boom,Random.Range(8f,15f));

			ScreenEffects.Instance.Sleep(1f);
			ScreenEffects.Instance.SlowDownEffect(0.05f);

			if (DemoData.Recoil)
			{
				ScreenEffects.Instance.Shake(Random.insideUnitCircle*0.05f);
			}

			MyOutlineImage.color = myOutlineColor;
			nextBoomTime = Time.time + BoomCooldown;
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.collider.tag == "Enemy")
		{
			Die();
		}
		if (other.collider.tag == "Bullet")
		{
			other.gameObject.SetActive(false);
			Destroy(other.gameObject,Random.Range(0.5f,4f));
			Die();
		}
	}

	void Die()
	{
		if (DemoData.Sounds)
		{
			DeathAudioSource.pitch = DemoData.Inaccuracy ? Random.Range(0.75f,1.25f) : 1f;
			DeathAudioSource.Play();
		}
		if (DemoData.SlowDown)
		{
			ScreenEffects.Instance.Sleep(1f);
			ScreenEffects.Instance.SlowDownEffect(1f);
		}

		if (DemoData.Explosions)
		{
			//Detatch the explosion and play it then destroy it randomly in the future.
			ParticleSystem ps = Instantiate(FancyExplosion,transform.position,Quaternion.identity).GetComponent<ParticleSystem>();
			ps.transform.parent = null;
			ps.transform.localScale = Vector3.one;
			Destroy(ps.gameObject,Random.Range(0.5f,4f));
		}

		gameObject.SetActive(false);
		GameController.Instance.ResetGame();

		_transform.position = Vector3.zero;
		_transform.rotation = Quaternion.identity;
		_rb.velocity = Vector2.zero;
		_trail.Clear();
	}

	public void Respawn()
	{
		gameObject.SetActive(true);
		_col.enabled = false;
		StartCoroutine(RespawnCo());
	}

	IEnumerator RespawnCo()
	{
		yield return new WaitForSeconds(0.15f);
		_renderer.enabled = false;
		yield return new WaitForSeconds(0.15f);
		_renderer.enabled = true;
		yield return new WaitForSeconds(0.15f);
		_renderer.enabled = false;
		yield return new WaitForSeconds(0.15f);
		_renderer.enabled = true;
		yield return new WaitForSeconds(0.15f);
		_renderer.enabled = false;
		yield return new WaitForSeconds(0.15f);
		_renderer.enabled = true;
		yield return new WaitForSeconds(2f);
		_col.enabled = true;
	}

	void DoDemoMods()
	{
		fireRate = fireRate * (DemoData.MoreBullets ? 0.15f : 1);
		bulletSpeed = bulletSpeed * (DemoData.FasterBullets ? 2 : 1);
		moveForce = moveForce * (DemoData.FasterBullets ? 3f : 1);
		rotateSpeed = rotateSpeed * (DemoData.FasterBullets ? 2f : 1);

		_trail.enabled = DemoData.Trails;
	}

	IEnumerator ScaleBoom()
	{
		for (float i = 0; i < 1; i+=Time.deltaTime*3.5f)
		{
			boomCollider.radius = Mathf.Lerp(0f, 2.5f, i);
			yield return null;
		}
		boomCollider.enabled = false;
		yield return new WaitForSeconds(1f);
		_col.enabled = true;
	}
}
