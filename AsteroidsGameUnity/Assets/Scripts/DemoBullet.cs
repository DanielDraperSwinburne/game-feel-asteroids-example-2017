﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoBullet : MonoBehaviour 
{
	private DemoDataObject DemoData;
	public bool EnemyBullet; //Is this an enemy bullet? (Used for colour and for sounds

	public Sprite BiggerSprite; //second stage sprite
	public SpriteRenderer FlashSprite; //the muzzle flash trick is to put an almost white circle as the first frame of the animation
	private TrailRenderer _trail; //my trail to turn on when in trail phase
	private SpriteRenderer _renderer; //my renderer used to set the sprite
	private BoxCollider2D _collider; //my collider, I need to make this bigger when the bullets get bigger.
	private AudioSource _src; //my shoot audio source
	public AudioClip BassSound; //the clip that has more bass

	void Awake()
	{
		//get local references to all neccisary components on this object.
		_trail = GetComponent<TrailRenderer>();
		_renderer = GetComponent<SpriteRenderer>();
		_collider = GetComponent<BoxCollider2D>();
		_src = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () 
	{
		DemoData = GameController.Instance.DemoData;

		if (DemoData.Colour) _renderer.color = EnemyBullet ? Color.magenta : Color.cyan;

		if (DemoData.BiggerBullets)
		{
			_renderer.sprite = BiggerSprite;
			_collider.size *=4f;
		}
		_trail.enabled = DemoData.Trails;

		if (DemoData.MuzzleFlash && FlashSprite != null)
			StartCoroutine(Flasher());
		else
			if (FlashSprite != null) FlashSprite.enabled = false;

		if (DemoData.MoreBass && !EnemyBullet) _src.clip = BassSound;

		if (DemoData.Sounds)
		{
			_src.pitch = DemoData.Inaccuracy ? Random.Range(0.75f,1.25f) : 1f;
			_src.Play();
		}
	}

	IEnumerator Flasher()
	{
		yield return new WaitForEndOfFrame();
		FlashSprite.enabled = false;
	}
}
