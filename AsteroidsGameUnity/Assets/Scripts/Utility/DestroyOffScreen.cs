﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOffScreen : MonoBehaviour 
{
	private Vector3 camBottomLeft;
	private Vector3 camTopRight;

	//local object;
	private Transform _transform;
	private Vector3 _mySize;
	private bool dead;
	// Use this for initialization
	void Awake () 
	{
		camBottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0)); //get the bottom corner of the screen
		camTopRight = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0)); //get the top corner of the screen

		_transform = transform;
		_mySize = GetComponent<Collider2D>().bounds.extents*1.1f; //add a little bit of padding so the object sio definately off the screen
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		//if the object isn dead yet AND its off the left side of the screen (Plus the width of the collider) OR off the right side of the screen (Plus the width of the collider)
		if (!dead && _transform.position.x + _mySize.x < camBottomLeft.x || _transform.position.x - _mySize.x > camTopRight.x) 
		{
			Die();
		}

		//if the object isn dead yet AND its off the bottom side of the screen (Plus the width of the collider) OR off the top side of the screen (Plus the width of the collider)
		if (!dead && _transform.position.y + _mySize.y < camBottomLeft.y || _transform.position.y - _mySize.y > camTopRight.y)
		{
			Die();
		}
	}

	void Die()
	{
		if (dead) return;
		dead = true;
		gameObject.SetActive(false); //lets hide the game object
		Destroy(gameObject,1f); //then destroy in a second in the future
	}
}
