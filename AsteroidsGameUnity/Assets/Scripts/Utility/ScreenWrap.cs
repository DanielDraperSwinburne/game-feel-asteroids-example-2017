﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWrap : MonoBehaviour 
{
	private Vector3 camBottomLeft;
	private Vector3 camTopRight;
	private bool wrappingX;
	private bool wrappingY;



	//local object;
	private Transform _transform;
	private Vector3 _mySize;
	private TrailRenderer _trail;
	private Collider2D _collider;

	void Awake()
	{
		_trail = GetComponent<TrailRenderer>();
		_collider = GetComponent<Collider2D>();
		_mySize = _collider.bounds.extents*1.1f;
	}
	// Use this for initialization
	void Start () 
	{
		camBottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		camTopRight = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));

		_transform = transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		Vector3 currentPosition = _transform.position;
		//if the object isn wrapping on the X axis yet AND its off the left side of the screen (Plus the width of the collider) OR off the right side of the screen (Plus the width of the collider)
		if (!wrappingX && currentPosition.x + _mySize.x < camBottomLeft.x || currentPosition.x - _mySize.x > camTopRight.x)
		{
			currentPosition.x = -currentPosition.x;
			if (_trail != null) _trail.Clear();
			wrappingX = true;
		}
		else
			wrappingX = false;

		//if the object isn wrapping on the Y axis yet AND its off the bottom side of the screen (Plus the width of the collider) OR off the top side of the screen (Plus the width of the collider)
		if (!wrappingY && currentPosition.y + _mySize.y < camBottomLeft.y || currentPosition.y - _mySize.y > camTopRight.y)
		{
			currentPosition.y = -currentPosition.y; // minus the current position to get the inverse of it (+- Equals -, -- Equals +)
			if (_trail != null) _trail.Clear();
			wrappingY = true;
		}
		else
			wrappingY = false;

		_transform.position = currentPosition; //set out new current positions
	}
}
