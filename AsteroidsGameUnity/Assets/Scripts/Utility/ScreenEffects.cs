﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.PostProcessing;

public class ScreenEffects : MonoBehaviour 
{	
	public static ScreenEffects Instance;

	private DemoDataObject DemoData;
	public PostProcessingProfile PostProcessing; // A reference to the post proccessing stack profile (For changing bloom, and chromatic abberation)

	//Screen Shaker
	private bool shaking = false; //Are we currently shaking?
	private float currentShakeForce = 0f; //how hard are we currently shaking?
	private float shakeX = 0f; 
	private float shakeY = 0f; //the offsets of the current shake
	private Vector3 defPos; //camera default position
	private Transform _cam; //camera reference
	private Coroutine shakeCo; //the shake coroutine

	//Bloom Effects
	private BloomModel.Settings bloomSettings; //our copy of the bloom setting struct


	//SlowDown Effects
	private ChromaticAberrationModel.Settings CASettings; //out copy of the Chromatic abberation settings struct
	private AudioMixerSnapshot SlowSnapshot; //the audio snapshot for slowmo
	private AudioMixerSnapshot NormalSnapshot; //the audio snapshot for normal time
	private AudioMixerSnapshot LowPassSnapshot; //a lowpass if needed
	public AudioMixer Mixer; //the mixer we are modifying with the snapshots
	private bool slowed; //are we currently in slow mo?
	public float slowedScale = 0.2f; //how slow can you go?

	//microSleeps
	private bool sleeping; //are we sleeping?
	private float lastSleepTime = 0; //when did we last start sleeping?

	//music
	public AudioSource MusicSource; //our music source for music effects

	//Audio Effects
	public int channel = 0; //what channel do we look at sound data from?
	public float freqMult = 1f; //what should we multiply the frequency by?
	private int qSamples = 1024; //how many samples should we take from the sound buffer? (The more you take the more accurate data you get)
	[Range(0,1024)]
	public int botCutOff = 0; //low cut frequencies (do you want to include low spectrum sounds?)
	[Range(0,1024)]
	public int topCutOff = 1024; //high cut frequencies (do you want to include high spectrum sounds?)
	
	private float[] samples; //an array to store samples in by reference


	//Fade Effect
	private Material fadeMaterial = null;
    private bool isFading = false; //are we fading out?
    private Color _currentColor;
	public float FadeOutTime = 1.2f; //how long should the fade take?
	public Color fadeColor = new Color(0.01f, 0.01f, 0.01f, 1.0f);


	void Awake()
	{
		Instance = this; //singleton pattern
		NormalSnapshot = Mixer.FindSnapshot("Normal"); //Get a Reference to the Mixer Snapshots
		SlowSnapshot = Mixer.FindSnapshot("Slowmo");
		LowPassSnapshot = Mixer.FindSnapshot("LowPass");

		Time.timeScale = 1f; //make sure our time scale always starts as 1
		UpdateFixedTimeScale(); //make sure fixed time is fine too
		NormalSnapshot.TransitionTo(0.1f); //make sure we are in normal audio snapshot
		samples = new float[qSamples];
		fadeMaterial = new Material(Shader.Find("Sprites/Default")); //just use the basic sprites shader.

		defPos = transform.position; //set the default cam position
	}

	// Use this for initialization
	void Start () 
	{
		DemoData = GameController.Instance.DemoData;

		_cam = Camera.main.transform;

		InitDemoSettings(); //setup demo settings

	}
	
	// Update is called once per frame (LAST CALLED UPDATE FUNCTION)
	void LateUpdate () 
	{
		UpdateFixedTimeScale();
		if (DemoData.MusicBloom)
		{
			bloomSettings = PostProcessing.bloom.settings;
			bloomSettings.bloom.intensity = GetFrequency(); // set the intensity of the bloom to the frequency of the music
			PostProcessing.bloom.settings = bloomSettings; // Set The new Change
		}
	}



	//SCREEN SHAKE STUFF
	public void Shake(Vector2 force)
	{
		if (!shaking || force.sqrMagnitude > currentShakeForce) //if we are already shaking make sure its a stronger focer before we interupt
		{
			currentShakeForce = force.sqrMagnitude;
			if (shakeCo == null)
				shakeCo = StartCoroutine(shakeEffect(force));
			else
			{
				StopCoroutine(shakeCo);
				shakeCo = StartCoroutine(shakeEffect(force));
			}
		}
	}

	public void ShakeExplosion(Vector2 point, float strength) // shake based on an explosion point to push the camera away from that point
	{
		Vector2 force = ((Vector2)_cam.position - point).normalized * strength;
		if (!shaking || force.sqrMagnitude > currentShakeForce)
		{
			currentShakeForce = force.sqrMagnitude;
			shakeCo = StartCoroutine(shakeEffect(force));
		}
	}

	IEnumerator shakeEffect(Vector3 push) //do the screen shacke
	{
		shaking = true;
		float start = Time.time;
		float duration = Mathf.Clamp(push.x+push.y+push.z,0.2f,3f); //clam the duration to a max of 3sec, though we should never reach that!
		
		while (Time.time < start + duration) //while we are still shaking
		{         
			
			float percentComplete = (Time.time - start) / duration;         //calculate how far we are thought
			float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f); //the rate as which to return to zero zero
			
			// map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f; //get a random -1 - 1 (honestly don''y know whay i did it this way. Probably tired
			float y = x;
			x *= push.x * damper;
			y *= push.y * damper; //lets dampen the value so that we dont shake ferociously forever
			shakeX = x;
			shakeY = y;

			Vector3 shakeVec = new Vector3(shakeX,shakeY,0);
			_cam.position = defPos+shakeVec; //make the shake actuallly happen

			yield return null;
		}
		
		shakeX = 0;
		shakeY = 0;
		shaking = false;
		shakeCo = null;
	}

	public void SlowDownEffect(float time)
	{
		if (!slowed)
		{
			StartCoroutine(SlowDownCo(time));
		}
	}

	IEnumerator SlowDownCo(float time)
	{
		slowed = true;
		SlowSnapshot.TransitionTo(1.5f); //transition to slow sound snapshop
		for (float t = 0; t <= 1; t+=Time.unscaledDeltaTime*0.75f)
		{
			Time.timeScale = Mathf.Lerp(Time.timeScale, slowedScale, t); //lerp the timeScale to the  slowed rate
			UpdateFixedTimeScale();
			CASettings.intensity = t; //use the same tansition rate to bump up the Chromatic abberation intensity
			PostProcessing.chromaticAberration.settings  = CASettings; 
			yield return null;
		}

		yield return new WaitForSeconds(time);

		NormalSnapshot.TransitionTo(1.5f); //Essentially do everything in reverse.
		for (float t = 0; t <= 1; t+=Time.unscaledDeltaTime*0.75f)
		{
			Time.timeScale = Mathf.Lerp(Time.timeScale, 1f, t);
			UpdateFixedTimeScale();
			CASettings.intensity = 1f-t;
			PostProcessing.chromaticAberration.settings  = CASettings;
			yield return null;
		}

		slowed = false;
	}

	public static void UpdateFixedTimeScale()
	{
		Time.fixedDeltaTime = 1f/60f*Time.timeScale; //adjust the fixed time for the physics update to be in sync with update during slowdown/speed up;
	}


	public void Sleep(float time)
	{
		if (!sleeping || time > lastSleepTime) 
		{
			StopCoroutine(SleepCo(0));
			StartCoroutine(SleepCo(time));
		}
	}

	IEnumerator SleepCo(float time)
	{
		sleeping = true;
		Time.timeScale = 0f; //litterally pause the game for a length of time
		float dt = Time.fixedDeltaTime;
		//Time.fixedDeltaTime = 0f;
		yield return new WaitForSecondsRealtime(time);
		Time.timeScale = 1f; //unpause
		Time.fixedDeltaTime = dt;
		sleeping = false;
	}

	void InitDemoSettings()
	{
		PostProcessing.bloom.enabled = DemoData.Bloom;
		bloomSettings = PostProcessing.bloom.settings;
		bloomSettings.bloom.intensity = 0.25f; 
		PostProcessing.bloom.settings = bloomSettings; // Set The new Change

		PostProcessing.chromaticAberration.enabled = DemoData.SlowDown;
		CASettings = PostProcessing.chromaticAberration.settings;
		CASettings.intensity = 0f;
		PostProcessing.chromaticAberration.settings  = CASettings;// Set The new Change

		if (DemoData.Music)
		{
			MusicSource.Play();
		}
	}

	//returns the frequency of the music scaled down from 1000 samples to a value between 0 - 10;
	float GetFrequency()
	{
		MusicSource.GetOutputData(samples,channel); //get array of samples from channel on the music source
		if (samples == null) return 0;
		float tot = 0f;
		foreach (float flt in samples)
		{
			tot += Mathf.Abs(flt); //add each sample together to get the frequency
		}
		tot = tot/100f;
		return tot*freqMult; //return the modified value
	}


	//get the Root Squared Mean betweeen 0 -1 
	float GetRMS()
	{
		MusicSource.GetOutputData(samples,channel);
		float sum = 0f;
		for (int i = (int)botCutOff; i < (int)topCutOff; i++)
		{
			sum += samples[i]*samples[i]; //square the samples
		}
		return Mathf.Sqrt(sum/qSamples); //square root the average
	}


	public void FadeOut()
    {
        StartCoroutine(FadeOutCo());
    }

	IEnumerator FadeOutCo()
    {
        float elapsedTime = 0.0f;
        fadeMaterial.color = fadeColor;
        _currentColor = fadeColor;
        isFading = true;
        while (elapsedTime < FadeOutTime) //while fading out change the color of the black overlay toward 1
        {
            yield return null;
            elapsedTime += Time.deltaTime;
            _currentColor.a = Mathf.Clamp01((elapsedTime) / FadeOutTime);
            //fadeMaterial.color = color;
        }
    }
	/// <summary>
    /// Renders the fade overlay when attached to a camera object
    /// </summary>
    protected void OnPostRender()
    {
        if (isFading) //while we are fadding draw a simple black quad OVER everything with _current colour
        {
            GL.PushMatrix();
            GL.LoadOrtho();
            fadeMaterial.SetPass(0);
            GL.Begin(GL.QUADS);
            GL.Color(_currentColor);
            GL.Vertex3(0, 0, 0);
            GL.Vertex3(1, 0, 0);
            GL.Vertex3(1, 1, 0);
            GL.Vertex3(0, 1, 0);
            GL.End();
            GL.PopMatrix();
        }
    }
}
