﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour 
{
	public static GameController Instance; //Singleton of the game controller to be accessed from anywhere.
	public DemoDataObject DemoData; //reference to the DemoData ScriptableObject that saves the current phase/state of the game

	public Text ScoreField; //The score ui field
	public GameObject GameOverText; //The GameOver Text Game Object in the scene

	public Image[] lifeImages; //A reference to the life indicators
	[Range(0,3)]
	private int lives = 3; //how many lives do we have
	private int _score;
	private IEnumerator _scoreCoroutine; //lets keep a reference of the score so we can interupt if needed.

	[Header("Asteroids")]
	public int numberOfAsteroids; //how many do we want to start the game with?
	public GameObject AsteroidPrefab; //The prefab of asteroids to create
	public Transform[] spawnPositions; //Possible locations to spawn Asteroids
	private float nextAsteroidTime; //when to spawn next asteroid (This was an afterthought and uses same code as ships)

	[Header("EnemyShips")]
	public float minTimeBetweenSpawn; //how long minimum before we spawn a ship
	public float maxTimeBetweenSpawn; //how long maximum before we spawn a ship
	public GameObject EnemyShipPrefab; //the enemy ship
	private float nextShipTime = 0; // the Time.time that we will spawn the next ship

	[Header("Player")]
	[Tooltip("The Player In The Scene")]
	public GameObject PlayerReference; //who is out player when they die?
	private PlayerController plyCtrl; //player functions for reseting etc

	private Vector3 camBottomLeft; //screen to worldspace camera positions for spawning etc
	private Vector3 camTopRight;

	void Awake()
	{
		Instance = this; //Singleton patterns
	}

	// Use this for initialization
	void Start () 
	{
		nextShipTime = Time.time + Random.Range(minTimeBetweenSpawn,maxTimeBetweenSpawn); //set the next spawn for the enemy ships

		camBottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		camTopRight = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0)); //Get the cam world pos for spawning

		GameOverText.SetActive(false); //its not game over yet! turn the texy off just in case itś on
		plyCtrl = PlayerReference.GetComponent<PlayerController>(); //player script

		for (int i = 0 ; i < lifeImages.Length; i++) //deactivate/activate current lives sprites
		{
			if (DemoData.Colour) lifeImages[i].sprite = plyCtrl.ColorSprite;
			lifeImages[i].enabled = (i < lives) ? true : false;
		}

		DoDemoMods(); //make changes based on demo stage
		SetupAsteroids(); //spawn all the asteroids
		nextAsteroidTime = Time.time+Random.Range(minTimeBetweenSpawn*2,maxTimeBetweenSpawn*2)-Mathf.Clamp(Time.realtimeSinceStartup*0.001f,0,10); //when to spawn next asteroid
	}

	void SetupAsteroids()
	{
		for (int i = 0; i < numberOfAsteroids; i++)
			Instantiate(AsteroidPrefab,spawnPositions[Random.Range(0,spawnPositions.Length)].position,Quaternion.identity);
	}

	// Update is called once per frame
	void Update () 
	{
		if (Time.time > nextShipTime) //is it time to spawn a new ship?
		{
			bool left = (Random.Range(0,2) != 0); //randomly choose left or right side
			Vector3 spawnPos = new Vector3(left ? camBottomLeft.x : camTopRight.x, Random.Range(camBottomLeft.y, camTopRight.y),0); //get a spawn pos on left or right between the whole side of screen
			Instantiate(EnemyShipPrefab,spawnPos,Quaternion.identity);

			nextShipTime = Time.time + Random.Range(minTimeBetweenSpawn,maxTimeBetweenSpawn);
		}

		if (Time.time > nextAsteroidTime) //is it time to spawn a new asteroid?
		{
			bool left = (Random.Range(0,2) != 0); //randomly choose left or right side
			Vector3 spawnPos = new Vector3(left ? camBottomLeft.x : camTopRight.x, Random.Range(camBottomLeft.y, camTopRight.y),0); //pick a random Y pos
			Instantiate(AsteroidPrefab,spawnPos,Quaternion.identity); //spawn it
			nextAsteroidTime = Time.time+Random.Range(minTimeBetweenSpawn*2,maxTimeBetweenSpawn*2)-Mathf.Clamp(Time.realtimeSinceStartup*0.05f,0,10); //set the time until the next one to be a bit shorter as time goes on.
		}
	}

	public void ResetGame()
	{
		if (lives > 1) //if we have run out of lives don reset, just game over instead
			StartCoroutine(ResetCo());
		else
			StartCoroutine(GameOverCo());
	}

	IEnumerator ResetCo()
	{
		yield return new WaitForSeconds(1f);

		//Clear screen of all bullets
		GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
		foreach (GameObject bullet in bullets)
			Destroy(bullet);

		//remove a life and update the life images.
		lives -= 1;
		for (int i = 0 ; i < lifeImages.Length; i++)
			lifeImages[i].enabled = (i < lives) ? true : false;
		
		yield return new WaitForSeconds(0.5f);

		plyCtrl.Respawn(); //respawn the player
	}

	IEnumerator GameOverCo()
	{
		yield return new WaitForSeconds(2f);
		ScreenEffects.Instance.FadeOut(); //lets fade to black for game over
		yield return new WaitForSeconds(1.2f);
		ClearScreen(); // clear all bullets and asteroids once the screen is black

		//remove a life and update the life images.
		lives -= 1;
		for (int i = 0 ; i < lifeImages.Length; i++)
			lifeImages[i].enabled = (i < lives) ? true : false;

		yield return new WaitForSeconds(1f);

		GameOverText.SetActive(true); //show game over text

		yield return new WaitForSeconds(3f);

		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //reload the level
	}

	void ClearScreen()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet"); //find all the enemies and bullets and destroy them

		foreach (GameObject enemy in enemies)
			Destroy(enemy);
		foreach (GameObject bullet in bullets)
			Destroy(bullet);

	}

	public void AddScore(int score)
	{
		_score += score; //add to the total score

		if (!DemoData.Animations)
		{
			ScoreField.text = _score.ToString();
		}
		else
		{
			if(_scoreCoroutine != null) //if we are already running the score co rutine, cancel it and start a fresh one.
	            StopCoroutine(_scoreCoroutine);

			_scoreCoroutine = ScoreUp(score);
	        StartCoroutine(_scoreCoroutine);
        }
	}

	IEnumerator ScoreUp(int score)
	{
		ScoreField.color = score < 0 ? Color.red : Color.cyan; //currently there is no negative scores. but if there was lets make it red to convey BAD **Though Red means goof fortune in china**
		LeanTween.scale(ScoreField.gameObject,Vector3.one*1.5f,0.05f);
        yield return new WaitForSeconds(0.2f);

		LeanTween.textColor(ScoreField.rectTransform,Color.white, 0.5f);
		LeanTween.scale(ScoreField.gameObject,Vector3.one,0.5f);
        var incrementAmount = (float)score/10f; //10 frames until we make it to the score we attained. 
        var scoreToDisplay = _score - score;

        for (int i = 0; i < 10; i++)
        {
            scoreToDisplay += (int)incrementAmount; //tick the score up every frame until we reach our REAL score.

			ScoreField.text = scoreToDisplay.ToString();

            yield return new WaitForSeconds(0.05f); //wait a couple of frames
        }

		ScoreField.color = Color.white;
		ScoreField.text = _score.ToString();

        _scoreCoroutine = null;
	}

	void DoDemoMods()
	{
		//If We are in a certain Demo Phase multiply by a specific value. Really id someone else was working on this I woould make these public/imported from excel.
		numberOfAsteroids = numberOfAsteroids * (DemoData.MoreEnemies ? 3 : 1);
		minTimeBetweenSpawn = minTimeBetweenSpawn * (DemoData.MoreEnemies ? 0.25f : 1);
		maxTimeBetweenSpawn = maxTimeBetweenSpawn * (DemoData.MoreEnemies ? 0.25f : 1);
	}
}
