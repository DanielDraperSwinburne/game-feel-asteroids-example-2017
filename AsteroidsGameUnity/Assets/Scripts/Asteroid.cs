﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour 
{
	public Sprite[] sprites; //We will choose a random sprite when we spawn

	public float moveSpeedMin;
	public float moveSpeedMax;
	public float rotateSpeedMin;
	public float rotateSpeedMax;
	public int scoreValue = 50;

	private Vector2 _vel;
	private Rigidbody2D _rb;
	private SpriteRenderer _renderer;
	private TrailRenderer _trail;
	private float rSpeed;

	public int brokenPieces;
	private Vector3 myStartscale;
	private GameObject[] children;

	public ParticleSystem Explosion;
	public ParticleSystem FancyExplosion;

	private bool isDead;

	//Hit Anims
	private int hitsLeft;
	public Material flashMat;
	private Material originalMat;
	private bool flashing;

	//Sounds
	private AudioSource _src;

	private DemoDataObject DemoData;
	public GameObject RandomExplodePrefab;

	public Sprite[] spritesColor;
	public AudioClip BassHit;
	public GameObject ImpactPrefab;

	public Sprite[] DebrisSprites;
	public GameObject DebrisPrefab;

	void Awake()
	{
		//store any references that wee need
		_renderer = GetComponent<SpriteRenderer>();
		_rb = GetComponent<Rigidbody2D>();
		originalMat = _renderer.material;
		_trail = GetComponent<TrailRenderer>();
		_src = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () 
	{
		myStartscale = transform.localScale;
		DemoData = GameController.Instance.DemoData;

		hitsLeft = brokenPieces-1; //lets set our hit health to the number of peices minus 1

		if (sprites.Length > 0)
			_renderer.sprite = DemoData.Colour ? spritesColor[Random.Range(0,spritesColor.Length)] : sprites[Random.Range(0,sprites.Length)];
		_vel = Random.insideUnitCircle*Random.Range(moveSpeedMin,moveSpeedMax); //when I'm created, select a random velocity Direction * speed
		rSpeed = Random.Range(rotateSpeedMin,rotateSpeedMax); //choose random speed


		if (brokenPieces > 1) //are we allowed to have broken pieces? I only want to split if i have at least 2 pieces
		{
			children = new GameObject[brokenPieces]; //lets store out childrens so we can sendn 
			for (int i = 0; i < brokenPieces; i++)
			{
				children[i] = (GameObject)Instantiate(gameObject,transform);
				children[i].GetComponent<Asteroid>().brokenPieces = brokenPieces-1; //remove 1 from the amount of broken pieces the children can have.
				children[i].SetActive(false);
			}
		}

		_trail.enabled = DemoData.Trails;

		if (DemoData.Animations)
		{
			transform.localScale = Vector3.zero; //set out scale to zero
			LeanTween.scale(gameObject,myStartscale,0.5f).setEaseOutBounce(); //tween from zero to out current scale in half a second
		}
		if (DemoData.MoreBass) _src.clip = BassHit;
	}
	
	// Update is called once per physics step
	void FixedUpdate () 
	{
		_rb.velocity = _vel;
		_rb.rotation += rSpeed*(60*Time.fixedDeltaTime);
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.collider.tag == "Boom")
		{
			if (DemoData.ScreenShake) ScreenEffects.Instance.ShakeExplosion(other.contacts[0].point,0.1f);
			Die();
			if (DemoData.Impacts) 
			{
				var angle = Mathf.Atan2(other.contacts[0].normal.y, other.contacts[0].normal.x) * Mathf.Rad2Deg;
				Quaternion.AngleAxis(angle, Vector3.forward);
				GameObject imp = (GameObject)Instantiate(ImpactPrefab,other.contacts[0].point,Quaternion.AngleAxis(angle, Vector3.forward),transform);
				Destroy(imp, Random.Range(3f,6f));
			}
		}
		if (other.collider.tag == "Bullet")
		{
			
			if (DemoData.ScreenShake) ScreenEffects.Instance.ShakeExplosion(other.contacts[0].point,0.1f);
			if (DemoData.Impacts) 
			{
				var angle = Mathf.Atan2(other.contacts[0].normal.y, other.contacts[0].normal.x) * Mathf.Rad2Deg;
				Quaternion.AngleAxis(angle, Vector3.forward);
				GameObject imp = (GameObject)Instantiate(ImpactPrefab,other.contacts[0].point,Quaternion.AngleAxis(angle, Vector3.forward),transform);
				Destroy(imp, Random.Range(3f,6f));
			}
			other.gameObject.SetActive(false);
			Destroy(other.gameObject,Random.Range(0.5f,4f));
			if (hitsLeft > 0 && DemoData.MoreEnemies)
				HitAnim();
			else
				Die();
		}
	}

	void Die()
	{
		if (isDead) return;
		isDead = true;

		if (DemoData.Debris)
		{
			for (int i = 0; i < brokenPieces; i++)
			{
				GameObject go = Instantiate(DebrisPrefab,transform.position,Quaternion.identity);
				go.GetComponent<SpriteRenderer>().sprite = DebrisSprites[Random.Range(0, DebrisSprites.Length)];
				go.GetComponent<Rigidbody2D>().velocity = Random.insideUnitCircle*moveSpeedMax;
				go.GetComponent<Rigidbody2D>().angularVelocity = rotateSpeedMax;
			}
		}

		if (brokenPieces > 1)
		{
			for (int i = 0; i < brokenPieces; i++)
			{
				children[i].transform.parent = null;
				children[i].transform.localScale = myStartscale*0.5f;
				children[i].transform.position = transform.position;
				children[i].SetActive(true);
			}
		}

		if (DemoData.Explosions)
		{
			//Detatch the explosion and play it then destroy it randomly in the future.
			FancyExplosion.transform.parent = null;
			FancyExplosion.transform.localScale = Vector3.one;
			FancyExplosion.Play();
			FancyExplosion.GetComponent<AudioSource>().Play();
			Destroy(FancyExplosion.gameObject,Random.Range(0.5f,4f));
		}
		else
		{
			//Detatch the explosion and play it then destroy it randomly in the future.
			Explosion.transform.parent = null;
			Explosion.transform.localScale = Vector3.one;
			Explosion.Play();
			if (DemoData.Sounds) Explosion.GetComponent<AudioSource>().Play();
			Destroy(Explosion.gameObject,Random.Range(0.5f,4f));
		}

		if (DemoData.Explosions && Random.value < 0.35f) 
		{
			GameObject p = (GameObject)Instantiate(RandomExplodePrefab,transform.position,Quaternion.identity);
			Destroy(p,Random.Range(0f,6f));
		}
		//Destroy me randomly in the future so we dont get frame drops if there is losts of things getting destroyed
		gameObject.SetActive(false);
		Destroy(gameObject, Random.Range(0.5f,4f));

		GameController.Instance.AddScore(scoreValue);
	}


	void HitAnim()
	{
		hitsLeft--;
		if (!flashing && DemoData.HitAnimations) StartCoroutine(Flash());
		if (DemoData.MicroSleep) ScreenEffects.Instance.Sleep(0.01f);
		if (DemoData.HitAnimations)
		{
			_src.pitch = DemoData.Inaccuracy ? Random.Range(0.75f,1.25f) : 1f;
			_src.Play();
		}
	}

	IEnumerator Flash()
	{
		flashing = true;
		_renderer.material = flashMat;
		yield return new WaitForSeconds(0.05f);
		_renderer.material = originalMat;
		flashing = false;
	}

}
