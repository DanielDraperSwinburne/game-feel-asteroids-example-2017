﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class DemoController : MonoBehaviour
{
	public DemoDataObject DemoData;
	public Text DemoPhaseText;
	public Animator _demoAnim;

	void Awake()
	{
		DemoPhaseText.text = DemoData.CurrentPhaseText;
		_demoAnim.SetTrigger("show");
	}

	void Update()
	{
		if (Input.GetButtonDown("Quit")) //if the Quit button is pressed Quit the game
		{
			Application.Quit();
		}

		if (Input.GetButtonDown("Restart")) //If the restart button is pressed reload the scene;
		{
			Reload();
			return;
		}

		if (Input.GetButtonDown("NextPhase")) //move onto the next phase
		{
			if (DemoData.CurrentPhaseNumber < 22)
				DemoData.CurrentPhaseNumber += 1;
			ChangePhase();
		}

		if (Input.GetButtonDown("PreviousPhase")) //move to the previous phase
		{
			if (DemoData.CurrentPhaseNumber > 0)
				DemoData.CurrentPhaseNumber -= 1;
			ChangePhase();
		}

		if (Input.GetButtonDown("FirstPhase")) //go to the start
		{
			DemoData.CurrentPhaseNumber = 0;
			BasePhase();
			return;
		}

		if (Input.GetButtonDown("LastPhase")) //go to the end
		{
			DemoData.CurrentPhaseNumber = 22;
			MusicBloomPhase();
			return;
		}


	}

	public void ChangePhase()
	{
		switch (DemoData.CurrentPhaseNumber) //massive switch to choose what config to use. 
		{
			case 0:
				BasePhase();
				break;
			case 1:
				ColourPhase();
				break;
			case 2:
				AnimationPhase();
				break;
			case 3:
				SoundPhase();
				break;
			case 4:
				MoreBulletsPhase();
				break;
			case 5:
				MoreEnemiesPhase();
				break;
			case 6:
				HitAnimationsPhase();
				break;
			case 7:
				InaccuracyPhase();
				break;
			case 8:
				BiggerBulletsPhase();
				break;
			case 9:
				FasterBulletsPhase();
				break;
			case 10:
				MuzzleFlashPhase();
				break;
			case 11:
				ImpactsPhase();
				break;
			case 12:
				ScreenShakePhase();
				break;
			case 13:
				MicroSleepPhase();
				break;
			case 14:
				RecoilPhase();
				break;
			case 15:
				TrailsPhase();
				break;
			case 16:
				MoreBassPhase();
				break;
			case 17:
				ExplosionsPhase();
				break;
			case 18:
				MusicPhase();
				break;
			case 19:
				DebrisPhase();
				break;
			case 20:
				SlowMoPhase();
				break;
			case 21:
				BloomPhase();
				break;
			case 22:
				MusicBloomPhase();
				break;
		}
	}

	/// <summary>
	/// Everything below this point is just config for each of the phases
	/// The reason I didn only turn bools on and off as I go is bevause I wanted to be able to jump to any phase at any time.
	/// </summary>

	public void BasePhase()
	{
		DemoData.CurrentPhaseText = "Asteroids";

		DemoData.Colour = false;
		DemoData.Animations = false;
		DemoData.Sounds = false;
		DemoData.MoreBullets = false;
		DemoData.MoreEnemies = false;
		DemoData.Inaccuracy = false;
		DemoData.BiggerBullets = false;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = false;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void ColourPhase()
	{
		DemoData.CurrentPhaseText = "Colour & Sprites";

		DemoData.Colour = true;
		DemoData.Animations = false;
		DemoData.Sounds = false;
		DemoData.MoreBullets = false;
		DemoData.MoreEnemies = false;
		DemoData.Inaccuracy = false;
		DemoData.BiggerBullets = false;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = false;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void AnimationPhase()
	{
		DemoData.CurrentPhaseText = "Basic Animations";

		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = false;
		DemoData.MoreBullets = false;
		DemoData.MoreEnemies = false;
		DemoData.Inaccuracy = false;
		DemoData.BiggerBullets = false;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = false;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void SoundPhase()
	{
		DemoData.CurrentPhaseText = "Sound Effects";

		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = false;
		DemoData.MoreEnemies = false;
		DemoData.Inaccuracy = false;
		DemoData.BiggerBullets = false;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = false;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void MoreBulletsPhase()
	{
		DemoData.CurrentPhaseText = "More Bullets";

		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = false;
		DemoData.Inaccuracy = false;
		DemoData.BiggerBullets = false;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = false;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void MoreEnemiesPhase()
	{
		DemoData.CurrentPhaseText = "More Enemies/Health";

		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = false;
		DemoData.BiggerBullets = false;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = false;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void HitAnimationsPhase()
	{
		DemoData.CurrentPhaseText = "Hit Animations";

		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = false;
		DemoData.BiggerBullets = false;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = true;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void InaccuracyPhase()
	{
		DemoData.CurrentPhaseText = "Bullet Inaccuracy";

		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = false;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = true;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void BiggerBulletsPhase()
	{
		DemoData.CurrentPhaseText = "Bigger Bullets";

		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = false;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = true;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void FasterBulletsPhase()
	{
		DemoData.CurrentPhaseText = "Faster Player/Bullets";

		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = false;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = true;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void MuzzleFlashPhase()
	{
		DemoData.CurrentPhaseText = "Muzzle Flash";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = false;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = true;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void ImpactsPhase()
	{
		DemoData.CurrentPhaseText = "Impact Effects";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = false;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = true;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void ScreenShakePhase()
	{
		DemoData.CurrentPhaseText = "Screen Shake";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = false;
		DemoData.HitAnimations = true;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void MicroSleepPhase()
	{
		DemoData.CurrentPhaseText = "Micro Sleeps";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = false;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void RecoilPhase()
	{
		DemoData.CurrentPhaseText = "Recoil";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = false;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void TrailsPhase()
	{
		DemoData.CurrentPhaseText = "Trails";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = true;
		DemoData.MoreBass = false;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void MoreBassPhase()
	{
		DemoData.CurrentPhaseText = "More Bass";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = true;
		DemoData.MoreBass = true;
		DemoData.Explosions = false;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void ExplosionsPhase()
	{
		DemoData.CurrentPhaseText = "Random Explosions";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = true;
		DemoData.MoreBass = true;
		DemoData.Explosions = true;
		DemoData.Music = false;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void MusicPhase()
	{
		DemoData.CurrentPhaseText = "Music";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = true;
		DemoData.MoreBass = true;
		DemoData.Explosions = true;
		DemoData.Music = true;
		DemoData.Debris = false;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void DebrisPhase()
	{
		DemoData.CurrentPhaseText = "Permanence";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = true;
		DemoData.MoreBass = true;
		DemoData.Explosions = true;
		DemoData.Music = true;
		DemoData.Debris = true;
		DemoData.SlowDown = false;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void SlowMoPhase()
	{
		DemoData.CurrentPhaseText = "Slow Motion Effects";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = true;
		DemoData.MoreBass = true;
		DemoData.Explosions = true;
		DemoData.Music = true;
		DemoData.Debris = true;
		DemoData.SlowDown = true;
		DemoData.Bloom = false;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void BloomPhase()
	{
		DemoData.CurrentPhaseText = "Bloom";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = true;
		DemoData.MoreBass = true;
		DemoData.Explosions = true;
		DemoData.Music = true;
		DemoData.Debris = true;
		DemoData.SlowDown = true;
		DemoData.Bloom = true;
		DemoData.MusicBloom = false;

		Reload();
	}

	public void MusicBloomPhase()
	{
		DemoData.CurrentPhaseText = "Music Synced Effects";
		DemoData.Colour = true;
		DemoData.Animations = true;
		DemoData.Sounds = true;
		DemoData.MoreBullets = true;
		DemoData.MoreEnemies = true;
		DemoData.Inaccuracy = true;
		DemoData.BiggerBullets = true;
		DemoData.FasterBullets = true;
		DemoData.MuzzleFlash = true;
		DemoData.Impacts = true;
		DemoData.ScreenShake = true;
		DemoData.MicroSleep = true;
		DemoData.HitAnimations = true;
		DemoData.Recoil = true;
		DemoData.Trails = true;
		DemoData.MoreBass = true;
		DemoData.Explosions = true;
		DemoData.Music = true;
		DemoData.Debris = true;
		DemoData.SlowDown = true;
		DemoData.Bloom = true;
		DemoData.MusicBloom = true;

		Reload();
	}

	void Reload()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
