﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Demo data object.
/// This stores the current state of the demo controller and carries it over when the scene is reloaded.
/// I also used this as a checklist for what features I had added.
/// </summary>

[CreateAssetMenu(fileName = "DemoDataObject", menuName = "Demo/DataObject", order = 1)]
public class DemoDataObject : ScriptableObject
{
	public string CurrentPhaseText = "Asteroids";
	public int CurrentPhaseNumber = 0;

	public bool Colour = false; //done
	public bool Animations = false; //done
	public bool Sounds = false; //done
	public bool MoreBullets = false; //done
	public bool MoreEnemies = false; //done
	public bool Inaccuracy = false; //done
	public bool BiggerBullets = false; //done
	public bool FasterBullets = false; //done
	public bool MuzzleFlash = false; //done
	public bool Impacts = false; //done
	public bool ScreenShake = false; //done
	public bool MicroSleep = false; //done
	public bool HitAnimations = false; //done
	public bool Recoil = false; //done
	public bool Trails = false; //done
	public bool MoreBass = false; //done
	public bool Explosions = false; //done
	public bool Music = false; //done
	public bool Debris = false; //done
	public bool SlowDown = false; //done
	public bool Bloom = false; //done
	public bool MusicBloom = false; //done
}
