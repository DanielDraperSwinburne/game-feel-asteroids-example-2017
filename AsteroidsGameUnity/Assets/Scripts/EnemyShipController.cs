﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipController : MonoBehaviour 
{
	public float moveSpeed; //my move speed
	public float timeBetweenDirUpdate = 2f; // how long between my direction changes in seconds
	public float fireRate = 2.5f; //the time between being able to fire
	public int scoreValue = 200; // how much score am I worth?
	public GameObject BulletPrefab; //my bullet prefab to use for shooting.
	private Transform _target; //a reference to the players transform so that I can target them

	private Vector2 _vel; //store my current velocity so that I only go one speed.
	private Rigidbody2D _rb; //my rigidbody
	private SpriteRenderer _renderer; //my renderer

	private float nextDirChange; //what time should I make my next direction change in Time.time.
	private float nextShoot; //similar to above, what time should I take my next shot in Time.time;
	public ParticleSystem Explosion; //my explosion effect
	public ParticleSystem FancyExplosion; //my fancy explosion effect;

	private bool isDead; //Am I dead yet?

	//Hit Anims 
	private int hitsLeft; //how many more hits before I die?
	public Material flashMat; //a white material for flashing when hit
	private Material originalMat; //my original material 
	private bool flashing; //am I currently flashing? I don want to trigger a flash during another flash otherwise itś possible to have a pure white enemy

	private DemoDataObject DemoData; //a reference to the demo data that I get at runtime

	private AudioSource _src; //my audio source
	public AudioClip BassHit; //my bass hit sound effect

	public Sprite ColorSprite; //my colour sprite

	void Awake()
	{
		//get local references to all neccisary components on this object.
		_rb = GetComponent<Rigidbody2D>();
		_renderer = GetComponent<SpriteRenderer>();
		_src = GetComponent<AudioSource>();
		//how many hits should I take?
		hitsLeft = 2;
		originalMat = _renderer.material; //lets save our normal material so that we can put it back after flashing 
	}

	// Use this for initialization
	void Start () 
	{
		DemoData = GameController.Instance.DemoData; //Lets get a reference to the Demo data from the game controller

		_renderer.sprite = DemoData.Colour ? ColorSprite : _renderer.sprite; //should we use our colour sprite yet


		GameObject ply = GameObject.FindGameObjectWithTag("Player"); //Find the player in the scene (There is only one object tagged as Player so this is 
		if (ply == null)
		{
			gameObject.SetActive(false); //If there is no player active, we should just Die. It will be easier than flooding the screen.
			Destroy(gameObject,1f);
		}
		else
		{
			_target = ply.transform;
			UpdateTargetDir();
			nextShoot = Time.time + fireRate;
		}
		if (DemoData.MoreBass) _src.clip = BassHit; 
	}
	
	// Update is called once per physics step
	void FixedUpdate () 
	{
		if (Time.time > nextDirChange) //Should I change direction again yet?
			UpdateTargetDir(); 

		_rb.velocity = _vel;

		if (Time.time >= nextShoot) //can we shoot yet?
		{
			//pew pew pew
			GameObject Bullet = (GameObject)Instantiate(BulletPrefab,transform.position,transform.rotation); //Spawn the bullet
			var dir = _target.position - transform.position;
 			var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg; //get the rotation the bullet needs to face to look normal.
			Bullet.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			Bullet.GetComponent<Rigidbody2D>().velocity = ((Vector2)_target.position-(Vector2)transform.position).normalized*2f;
			nextShoot = Time.time + fireRate; // next time we can fire is fire rate into the future
		}
	}

	void UpdateTargetDir()
	{
		Vector2 dirToTar = ((Vector2)_target.position+Random.insideUnitCircle.normalized)-(Vector2)transform.position; //vector math for a difference is also a directions to the object
		_vel = (Vector2)dirToTar.normalized*moveSpeed;
		nextDirChange = Time.time + timeBetweenDirUpdate;
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.collider.tag == "Boom")
		{
			if (DemoData.ScreenShake) ScreenEffects.Instance.ShakeExplosion(other.contacts[0].point,0.1f);
			Die();
		}

		if (other.collider.tag == "Bullet") //did we get hit by a bullet?
		{
			if (DemoData.ScreenShake) ScreenEffects.Instance.ShakeExplosion(other.contacts[0].point,0.15f); //cause screen shake from this point on the screen with this strength
			other.gameObject.SetActive(false);
			Destroy(other.gameObject,Random.Range(0.5f,4f));
			if (hitsLeft > 0 && DemoData.MoreEnemies) //if we have health left, jsut play the hit animation animation OTHERWISE i should die.
				HitAnim();
			else
				Die();
		}
	}

	void Die()
	{
		if (isDead) return; //did we accidently get called twice?

		isDead = true;

		if (DemoData.Explosions)
		{
			//Detatch the explosion and play it then destroy it randomly in the future.
			FancyExplosion.transform.parent = null; //remove it from its parent so that it doesn travel or get disabled
			FancyExplosion.transform.localScale = Vector3.one; //make sure itś scale is 1,1,1
			FancyExplosion.Play(); //play the particle
			FancyExplosion.GetComponent<AudioSource>().Play(); //player the audio
			Destroy(FancyExplosion.gameObject,Random.Range(0.5f,4f)); //destroy it some time in the future
		}
		else
		{
			//Detatch the explosion and play it then destroy it randomly in the future.
			Explosion.transform.parent = null;
			Explosion.transform.localScale = Vector3.one;
			Explosion.Play();
			if (DemoData.Sounds) Explosion.GetComponent<AudioSource>().Play();
			Destroy(Explosion.gameObject,Random.Range(0.5f,4f));
		}

		//Destroy me randomly in the future so we dont get frame drops if there is losts of things getting destroyed
		gameObject.SetActive(false); 
		Destroy(gameObject, Random.Range(0.5f,4f));

		GameController.Instance.AddScore(scoreValue); //We died Add score to the players score
	}

	void HitAnim()
	{
		hitsLeft--;
		if (!flashing && DemoData.HitAnimations) StartCoroutine(Flash()); //if we can flash start the flash co-routine
		//micro sleep of 1/100th of a second (It takes about 220ms for the human brain to fully process an input, this extends that time to add wieght)
		if (DemoData.MicroSleep) ScreenEffects.Instance.Sleep(0.01f); 
		if (DemoData.HitAnimations)
		{
			_src.pitch = DemoData.Inaccuracy ? Random.Range(0.75f,1.25f) : 1f; //if we have innacuracy change the pitch of the sound so they all sound a little different.
			_src.Play();
		}
	}

	IEnumerator Flash()
	{
		flashing = true;
		_renderer.material = flashMat;
		yield return new WaitForSeconds(0.05f);
		_renderer.material = originalMat;
		flashing = false;
	}
}
